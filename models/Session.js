
/**
 * This model can be used to track sessions. If I put it into use, I would
 * have to make a sessionStore for passport that will add/remove session 
 * entries appropriately.
 * This could be used for a 'currently signed in devices' with the ability
 * to revoke sessions, view a session log, etc.
 */
module.exports = function (app) {
  return function(sequelize, DataTypes) {

    var modelAttributes = {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      // user_id: {
      //   type: DataTypes.INTEGER
      // },
      ip: {
        type: DataTypes.STRING
      },
      user_agent: {
        type: DataTypes.STRING
      }
    };

    // The attributes in this model should be build from the config
    var Session = sequelize.define('Session', modelAttributes, {
      paranoid: true ,
      tableName: 'sessions' ,
      comment: 'Session database comment' ,
      associate: function(models) {
        Session.belongsTo(models.User, { foreignKey: 'user_id' });
      },
      classMethods: {
        // attributeWhitelist: function(){ return [] }
      },
      instanceMethods: {
        // Note: cb will receive (err, Boolean)
        // getFullname: function() {
        //   return [this.first_name, this.last_name].join(' ');
        // }
      }
    });

    Session.beforeValidate( function(session, fn) {
      // logger('beforeValidate - Session');
      return fn(null, session);
    });
    Session.afterValidate( function(session, fn) {
      // logger('afterValidate - Session');
      return fn(null, session);
    });
    Session.beforeCreate( function(session, fn) {
      // logger('beforeCreate - Session');
      return fn();
    });
    Session.afterCreate( function(session, fn) {
      // logger('afterCreate - Session');
      return fn();
    });
    Session.beforeUpdate( function(session, fn) {
      // logger('beforeUpdate - Session');
      // If password has changed, rehash it
      return fn();
    });
    Session.afterUpdate( function(session, fn) {
      // logger('afterUpdate - Session');
      return fn();
    });
    Session.beforeDestroy( function(session, fn) {
      // logger('beforeDestroy - Session');
      return fn();
    });
    Session.afterDestroy( function(session, fn) {
      // logger('afterDestroy - Session');
      return fn();
    });

    return Session;

  };
};
