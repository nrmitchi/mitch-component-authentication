
var _ = require('lodash') ;
var _str = require('underscore.string') ;
var randomString = require('./random_string') ;
var async = require('async') ;

var debug = require('debug')('mitch:component:authentication:accountLinker') ;

/**
 * Todo: Cleanup using async (probably waterfall)
 * Todo: Figure out what my .error should do, and pull it out into one function. Be more DRY
 * Todo: Somehow do a configurable function which will run when a new account is being created/linked
 *       Similar to my warp onStartup function, and this will have a signature of function (user, socialAccount, profile, done)
 *       A user could use this to store extra information that he cares about. In fact, I may end up saving the profile object as TEXT just incase anyways
 */

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function fixUsername(attr) {
  return attr + getRandomInt(0,9);
}

module.exports = function (app) {

  function ensureUnique (obj, attribute, fixup, cb) {
    // Annoying that I have to do this this way...
    var criteria = {};
    criteria[attribute] = obj[attribute];
    app.models.User.count({where: criteria})
      .then(function (count) {
        if (count === 0) {
          return cb();
        } else {
          // Username is taken, add a number
          obj[attribute] = fixup(obj[attribute]);
          ensureUnique(obj, attribute, fixup, cb);
        }
      })
      .catch( function (err) {
        debug(err);
        cb(err);
      });
  }

  return function(req, query, profile, done) {
    var accessToken  = query.tokens.accessToken;
    var refreshToken = query.tokens.refreshToken;
    var identifier   = query.identifier;
    var provider     = profile.provider;
    var protocol     = query.protocol;
    
    // Create Social Login linked to account
    var UserCredential = app.models.UserCredential;
    var User = app.models.User;
    debug('Linking/creating account...');

    /**
     * getEmails()
     *
     * Responsible for determining the email address(es) of an authenticated
     * user, and pass them to the callback.
     *
     * Callback is called with the signature (err, emails)
     *
     * @param {Function} cb
     */
    function getEmails(cb) {
      // If the profile object contains a list of emails, grab the first one and
      // add it to the user.
      if (profile.hasOwnProperty('emails')) {
        // user.email = profile.emails[0].value;
        cb(null, profile.emails); //['doesnotexist@nrmitchi.ca'])
      } else {
        // if (provider == 'github') {
        //   // make request to https://api.github.com/user/emails?access_token=$accessToken to get emails
        // } else if (provider == 'bitbucket') {
        //   // get emails from https://bitbucket.org/api/1.0/users/$username/emails
        // } else {
        //   cb(null, profile.emails)
        // }
        cb(null, ['doesnotexist@devnull.com']);
      }
    }
    /**
     * findOrCreateUser()
     *
     * Accepts a list of email addresses and finds the 'first' user with an
     * email in the list. If no user is found, one is created.
     *
     * Callback is called with the signature (err, user)
     *
     * @param {Array}    emails
     * @param {Function} cb
     */
    function findOrCreateUser (emails, cb) {
      var email = emails[0].value;
      // Todo: Use findOrCreate (once it actually works)
      User.find({
        where: { email: email }
      }).then( function (user) {
        if (user) {
          debug('User -> Existing, Provider: '+provider);
          cb(null, user);
        } else {
          debug('User -> New, Provider: '+provider);

          // Attributes that are common throughout different providers
          var userAttributes = {
            email       : email , //, // I still need to set the scope for this
            password    : randomString(40) , // Because every account requires a password, a 40 digit random string will have to do.
            verified    : true , // Todo: Should depend on environment, and verification config
            givenName   : profile.name ? profile.name.givenName : null ,
            familyName  : profile.name ? profile.name.familyName : null
          };
          if (profile.username) {
            _.merge(userAttributes, {
              username    : profile.username , //_str.slugify(profile.name.givenName+' '+profile.name.familyName),
            });
          } else if (userAttributes.givenName && userAttributes.familyName) {
            _.merge(userAttributes, {
              username    : _str.slugify(userAttributes.givenName+'-'+userAttributes.familyName)
            });
          } else {
            // Random string username (can be switched later)
            _.merge(userAttributes, {
              username    : randomString(15)
            });
          }

          // Todo: abstract this into a function to ensure uniqueness of any attribute
          // Todo: Clean this up to use async
          // Ensure that username isn't already taken. If it is, add a random number?
          ensureUnique(userAttributes, 'username', fixUsername, function (err) {
            if (err) {
              debug(err);
              return cb(err);
            }

            // Create default email if the auth strategy didn't provide it. This really shouldn't happen though.
            // We use the site host for this for now
            if (!userAttributes.email) {
              userAttributes.email = userAttributes.username+'@'+app.config.authentication.baseUrl;
            }
            
            User.create(userAttributes)
              .then( function (user) {
                cb(null, user);
              })
              .catch( function (err) {
                debug(err);
                cb(err);
              });
          });
        }
      })
      .catch( function (err) {
        // An error occurred
        debug(err);
        cb(err);
      });
    }
    /**
     * createAndLinkUserCredential()
     *
     * Finds a UserCredential for the supplied user and login provider.
     * If one is not found, a new one is created, and linked to the user.
     * If one is found, the associated access/refresh tokens are updated.
     *
     * Callback is called with the signature (err, user)
     *
     * @param {Object}   user
     * @param {Function} cb
     */
    function createAndLinkUserCredential(user, cb) {
      // If the accoutn already exists and the access/refresh token has changed, update them
      // Try to switch this to findOrCreate, but waterline may have to be switched
      // I still don't know why this actually has to do this find...
      console.log({
          user_id: typeof user.id,
          provider: typeof provider,
          provider_id: typeof(identifier || profile.username)
        });
      UserCredential.find({
        where: {
          user_id: user.id,
          provider: provider,
          provider_id: ''+(identifier || profile.username)
        }
      }).then(function (sa) {
          if (!sa) {
            UserCredential.create({
              user_id         : user.id , // <-- Effectively does the linking
              provider        : provider ,
              provider_id     : identifier || profile.username ,// Bitbucket doesn't seem to provide an id; fall back to username
              access_token    : accessToken ,
              refresh_token   : refreshToken ,
              profile         : profile._json
            })
            .then(function (socialAccount) {
              cb(null, user);
            })
            .catch( function (err) {
              debug(err);
              cb(err);
            });
          } else {
            if ( accessToken != sa.access_token || refreshToken != sa.refresh_token ) {
              sa.access_token = accessToken;
              sa.refresh_token = refreshToken;

              sa.save()
                .then(function (err, sa) {
                  cb(null, sa);
                })
                .catch(cb);
            } else {
              cb(null, user);
            }
          }
        })
        .catch(function (err) {
          debug(err);
          cb(err);
        });
    }

    UserCredential.find({
      where: { provider: profile.provider, provider_id: ''+profile.id},
      include: [ User ]
    }).then( function (socialAccount) {
      // The account already exists; simply return the user
      if (socialAccount) {
        // Check if tokens need to be updated
        if (accessToken == socialAccount.access_token &&
              refreshToken == socialAccount.refresh_token) {
          done(null, socialAccount.User);
        } else {
          // Else the tokens have changed. Update them
          // socialAccount.update({
          //     access_token  : accessToken ,
          //     refresh_token : refreshToken
          //   })
          //   .then( function () {
          //     cb(null, socialAccount.User);
          //   })
          //   .catch( function (err) {
          //     debug(err)
          //     cb(err)
          //   })
          done(null, socialAccount.User);
        }
      } else {
        // Account is new. Create the social login. If a user exists with the same email, link them. Otherwise create new account
          // I should probably do this for all emails in the list at some point
        debug('Linking/creating account...');
        async.waterfall([
          getEmails ,
          findOrCreateUser ,
          createAndLinkUserCredential
        ], function (err, user) {
          if (err) debug(err);
          done(null, user);
        });
      }
    })
    .catch( function (err) {
      debug(err);
      done(err);
    });
  };
};
