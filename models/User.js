
var bcrypt = require('bcrypt') ;
var randomString = require('../utils/random_string') ;

// var ssaclAttributeRoles = require('ssacl-attribute-roles')

var debug = require('debug')('mitch:component:authentication:model:User');

module.exports = function (app) {
  return function (sequelize, DataTypes) {

    var modelAttributes = {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      givenName: {
        type: DataTypes.STRING
      },
      familyName: {
        type: DataTypes.STRING
      },

      username: {
        type: DataTypes.STRING,
        unique: true,
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      
      // Todo: Should probably move these off of the User model and into UserCredentials
      api_key: {
        type: DataTypes.STRING,
        allowNull: false
      },
      api_secret: {
        type: DataTypes.STRING,
        allowNull: false
      },
      
      admin: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      superuser: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      
      verified: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      verified_timestamp: {
        type: DataTypes.DATE
      },

      last_login: {
        type: DataTypes.DATE
      },
      login_count: {
        type: DataTypes.INTEGER,
        defaultValue: 0
      }

    };

    // The attributes in this model should be build from the config
    var User = sequelize.define('User', modelAttributes, {
      paranoid: true ,
      tableName: 'users' ,
      comment: 'User database comment' ,
      associate: function(models) {
        User.hasMany(models.UserCredential, { foreignKey: 'user_id' });
        // User.hasMany(models.Session, { foreignKey: 'user_id' })
      },
      classMethods: {
        // attributeWhitelist: function(){ return [] }
      },
      instanceMethods: {
        // Note: cb will receive (err, Boolean)
        checkPassword: function (password, cb) {
          if (this.get('password', { raw: true })) {
            bcrypt.compare(password, this.get('password', { raw: true }), cb);
          } else {
            cb(new Error('User does not have local password'), null);
          }
        },
        rollKeys: function (cb) {
          // Reset the API Key and Secret
          this.api_key = randomString(40);
          this.api_secret = randomString(60);
        },

        trackLogin: function (req, cb) {
          // Update User login_count, last_login, etc
          this.updateAttributes({
            last_login  : new Date() ,
            login_count : this.login_count + 1
          }, ['last_login', 'login_count'])
            .then( function () {})
            .catch( function (err) {
              debug('Error saving login: '+err);
            });
        }
        // getFullname: function() {
        //   return [this.first_name, this.last_name].join(' ');
        // }
      }
    });

    User.beforeValidate( function(user, options, fn) {
      debug('beforeValidate - User');

      // This is only here because beforeCreate does not run first.
       // Except on create, this should actually fail
      // Set keys if they are not set
      if (!user.api_key) user.api_key = randomString(40);
      if (!user.api_secret) user.api_secret = randomString(60);

      return fn(null, user);
    });
    User.afterValidate( function(user, options, fn) {
      debug('afterValidate - User');
      return fn(null, user);
    });
    User.beforeCreate( function(user, options, fn) {
      debug('beforeCreate - User');
      
      // Set keys if they are not set
      if (!user.get('api_key', { raw: true })) user.set({ api_key: randomString(40) }, { raw: true });
      if (!user.get('api_secret', { raw: true })) user.set({ api_secret: randomString(60) }, { raw: true });

      bcrypt.hash(user.get('password', { raw: true }), app.authentication.bcryptPasswordRounds || 6, function (err, hash) {
        user.set({ password: hash }, { raw: true });
        return fn();
      });
    });
    User.afterCreate( function(user, options, fn) {
      debug('afterCreate - User');
      if (app.isProd) {
        debug('  prod detected: Sending verification email');
      }
      return fn();
    });
    User.beforeUpdate( function(user, options, fn) {
      debug('beforeUpdate - User');
      // If password has changed, rehash it
      if (user.changed('password')) {
        bcrypt.hash(user.password, app.authentication.bcryptPasswordRounds || 6, function (err, hash) {
          user.set({ password: hash }, { raw: true }); // = hash;
          return fn();
        });
      } else {
        return fn();
      }
    });
    User.afterUpdate( function(user, options, fn) {
      debug('afterUpdate - User');
      return fn();
    });
    User.beforeDestroy( function(user, options, fn) {
      debug('beforeDestroy - User');
      return fn();
    });
    User.afterDestroy( function(user, options, fn) {
      debug('afterDestroy - User');
      return fn();
    });

    // ssaclAttributeRoles(User);

    return User;

  };
};
