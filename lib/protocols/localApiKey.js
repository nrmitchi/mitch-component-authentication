
var passport = require('passport');

module.exports = function (app) {
  return function(req, apiKey, done) {
    app.models.User.find({
      where: { api_key: apiKey }
    }).then( function (user) {
        if (!user) {
          return done(null, false, { message: 'Invalid API Key' });
        } else {

          user._authenticatedByAPIKey = true;

          return done(null, user, { _authenticatedByAPIKey : true });
        }
      })
      .catch(done);
  };
};
