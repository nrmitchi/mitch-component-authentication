
var mitch       = require('mitch') ;
var _           = require('lodash') ;

var passport    = require('passport') ;

var debug       = require('debug')('mitch:component:authentication:routes:Passport');

module.exports = function (app) {

  var localProtocol = require('../lib/protocols/local')(app);

  var config = app.config.authentication;
  var router = mitch.Router();

  function whitelistProviders (req, res, next) {
    // ensure that req.params.provider is a currently enabled strategy. If not, return 404
    // Todo: Pull from config (ie, keys of passport.strategies , but should ignore 'local')
    if (passport._provider_whitelist.indexOf(req.params.provider) > -1) {
      next();
    } else {
      res.status(404).end();
    }
  }

  router.get('/:provider', whitelistProviders, function (req, res, next) {
    /**
     * Authentication with the given provider, with the configured scope (or nothing)
     *
     * Todo: Use a better default scope
     */
    passport.authenticate(req.params.provider, {
      scope: config.passport.strategies[req.params.provider] ?
             config.passport.strategies[req.params.provider].scope : ''
      })(req, res, next);
  });

  // Todo: Check for locked/deliquent accounts on social logins
  router.get('/:provider/callback', whitelistProviders,
    function (req, res, next) {
      var scope = config.passport.strategies[req.params.provider] ? config.passport.strategies[req.params.provider].scope : '';
      passport.authenticate(req.params.provider, { scope: scope }, function(err, user, info) {
        if (err) {
          return res.redirect(app.config.protocol+'://'+app.config.authentication.redirectUrl+'/?login-failed');
        }
        if (!user) {
          return res.status(400).json(info);
        }

        req.logIn(user, function(err) {
          if (err) {
            return res.redirect(app.config.protocol+'://'+app.config.authentication.redirectUrl+'/?login-failed&err='+req.originalUrl);
          }

          return res.redirect(app.config.protocol+'://'+app.config.authentication.redirectUrl);
        });

      })(req, res, next);
    }
  );

  if (app.config.authentication.passport.strategies.local) {
    router.post('/local', function (req, res, next) {
      passport.authenticate('local', function (err, user, info) {
        if (err) {
          return next(err)
        }
        if (!user) {
          return res.status(400).json(info);
        }

        req.logIn(user, function(err) {
          if (err) {
            return next(err);
          }

          return res.status(200).json(user);
        });

      })(req, res, next);
    });
  }

  return router;

};
