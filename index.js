
// Export the mount path and server. Warp will looked for 'server' when mounting
module.exports = function(app) {

  app.Errors = app.Errors || {};
  app.Errors.Authentication = require('./errors');

  return {
    mountPath  : '/'+app.config.authentication.mountPath ,
    server     : require('./server.js')(app)
  };

};
