
/**
 * Notes: I can't remember why this is split into setup and mount,
 *    and I'm not sure if I can return a router here and mount it
 *    because I need to make sure auth is hit before anything else
 */
 
var _ = require('lodash') ,
    path = require('path') ,
    url = require('url') ,
    passport = require('passport') ,
    accountLinker = require('../utils/accountLinker') ,
    debug = require('debug')('mitch:component:authentication:loadPassportModules') ;

module.exports = function (app, config) {

  return function (cb) {

    cb = cb || function () {};
    
    var db     = app.models;
    var config = app.config.authentication;

    var authMountPath = app.config.authentication.mountPath || '_auth';

    // Passport session setup.
    //   To support persistent login sessions, Passport needs to be able to
    //   serialize users into and deserialize users out of the session.  Typically,
    //   this will be as simple as storing the user ID when serializing, and finding
    //   the user by ID when deserializing.
    passport.serializeUser(function(user, done) {
      debug('Serializing');
      if (user) {
        done(null, user.id);
      } else {
        done({ error: 'Unable to serialize user' }, false);
      }
    });

    passport.deserializeUser(function(id, done) {
      debug('Deserializing');

      var toEagerLoad = _.map(config.eagerLoad, function (relation) {
        return app.models[relation];
      });
      app.models.User.find({ where: { id : id }, include : toEagerLoad })
        .then( function (user) {
          return done(null, user);
        })
        .catch( function (err) {
          debug(err);
          done(err, null);
        });
    });

    passport.connect = accountLinker(app);

    passport.nosession = [];

    passport.checkNoSession = function checkNoSession (req, res, next) {

      // If a user already exists (likely from a session) skip this
      if (req.user) return next();

      // Basic, Digest, OAuth, etc
      passport.authenticate(passport.nosession, { session: false }, function (err, user, info) {
        /*
         * For now, log the error and continue (most likely unauthenticated)
         * Note: This behaviour may change
         */ 
        if (err) {
          console.log(err);
        }

        if (user) {
          // Set the user to the request. Because we are using a custom callback, passport
          // does not do this automatically
          req.user = user;

          if (info && info._authenticatedByAPIKey) req._authenticatedByAPIKey = true;
          // Inject checkSignature, which will reject invalid requests outside of dev mode
            // If in dev mode, just assume signature is correct. I should think of a better way
          if (app.isDev) {
            debug('Development mode; ignoring signature check');
            return next();
          } else {
            if (app.config.authentication.checkSignatures) {
              app.middleware.checkSignature(req, res, next);
            } else {
              next();
            }
          }
          
        } else {
          next();
        }
      })(req, res, next);
    };

    passport.loadStrategies = _.bind(function () {
      var passport   = this ,
          protocols  = require('./protocols') ,
          strategies = app.config.authentication.passport.strategies ;

      debug('  Mounting Auth:');
      Object.keys(strategies).forEach(function (key) {
        var options = { passReqToCallback: true } ,
            Strategy;

        // If this session
        if (strategies[key].nosession) passport.nosession.push(key.toLowerCase());

        if (key === 'local') {
          // Since we need to allow users to login using both usernames as well as
          // emails, we'll set the username field to something more generic.
          _.extend(options, strategies[key].options);

          // Only load the local strategy if it's enabled in the config
          if (strategies.local) {
            Strategy = strategies.local.strategy;

            passport.use(new Strategy(options, protocols.local(app).login));
          }
        } else if (_.contains(['localApiKey', 'bearer', 'basic'], key)) {

          Strategy = strategies[key].strategy;

          _.extend(options, strategies[key].options);

          passport.use(new Strategy(options, protocols[key](app)));

        } else {
          var protocol = strategies[key].protocol
            , callback = strategies[key].callback;

          // Add to provided list to prevent 404s
          passport._provider_whitelist.push(key);

          if (!callback) {
            callback = path.join(authMountPath, key, 'callback');
          }

          Strategy = strategies[key].strategy;

          var baseUrl = app.config.protocol+'://'+app.config.host; //getBaseurl().replace(':10000','');

          switch (protocol) {
            case 'oauth':
            case 'oauth2':
              options.callbackURL = url.resolve(baseUrl, callback);
              break;

            case 'openid':
              options.returnURL = url.resolve(baseUrl, callback);
              options.realm     = baseUrl;
              options.profile   = true;
              break;
          }

          // Merge the default options with any options defined in the config. All
          // defaults can be overriden, but I don't see a reason why you'd want to
          // do that.
          _.extend(options, strategies[key].options);

          passport.use(new Strategy(options, protocols[protocol]));
        }

        debug('    -> %s', key);

      });
    }, passport);

    // This should actually generate a list of which providers are used from the config
    passport._provider_whitelist = [];

    app.middleware('session', passport.initialize());
    app.middleware('session', passport.session());
    app.middleware('session', passport.checkNoSession);
    
    // Actually load the used strategies into passport.
    passport.loadStrategies();

    // This should probably be moved into the local strategy mounting
    if (config.passport && config.passport.strategies && config.passport.strategies.local && config.passport.strategies.local.remember_me) {
      app.use(passport.authenticate('remember-me'));
    }

    return cb();

  };
};
