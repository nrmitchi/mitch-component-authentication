
var mitch       = require('mitch') ;

var debug = require('debug')('mitch:component:authentication:routes:Session');

module.exports = function (app) {

  var config = app.config;

  // Todo: Consider tapping .Router() onto app so that I can avoid imports
  //       Creating new Routers is common so...
  var router = mitch.Router();
  
  /**
   * Retrieve information about all current sessions for the user.
   * Note that (for now at least) this will only be one session
   *
   * These are in place as they are though to support multiple sessions at the same time
   * and a user account being able to cancel other sessions, etc.
   *
   * Yes, this will require a 'sessions' database table
   */
  router.get('/', [ app._middleware.requireUser ], function (req, res) {
    // Return information about the current users sessions
    res.json({ session: req.session });
    // Select all sessions from the session store/db archive that belong to the current user.
  });
  // app.post('/session', ... ) will create a session if local strategy is used
  /**
   * Return the current session information
   */
  router.get('/current', [ app._middleware.requireUser ], function (req, res) {
    // Return information about the current users current sessions
    // Sign in time
    // IP
    // Session expiry time
    // anything else that I track
    res.json({ session: req.session });
  });
  /**
   * Destroy the current session. Equivalent to POST /logout
   */
  router.delete('/', function destroySession (req, res) {
    req.logout();
    res.status(204).end();
  });

  // if (warp.config.authentication.useLegacySessionEndpoints) {
  //   /**
  //    * In reality these functions will be in a controller so there will not
  //    * repeated code
  //    */
    
  //   router.get('/logout', function destroySession (req, res) {
  //     req.logout();
  //     res.redirect(warp.config.protocol+"://"+warp.config.host);
  //   });

  //   router.post('/logout', function destroySession (req, res) {
  //     req.logout();
  //     res.status(204).end();
  //   });
  // }

  return router;

};
