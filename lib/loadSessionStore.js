
var assert          = require('assert') ,
    Redis           = require('redis') ,
    expressSession  = require('express-session') ,
    RedisStore      = require('connect-redis')(expressSession) ,
    flash           = require('connect-flash') ;

module.exports = function (app, config) {

  return function (cb) {

    cb = cb || function () {};

    assert(config.session.host);
    assert(config.session.port);

    var redisClient = Redis.createClient( config.session.port, config.session.host );

    /**
     * If auth information is set, then we send the AUTH packets to Redis
     */
    if (config.session.auth) {
      redisClient.auth(config.session.auth);
    }

    app.authentication.sessionStore = new RedisStore({ client: redisClient }) ;

    app.middleware('initial', flash());

    app.middleware('session:before', expressSession({
      name: config.session.name || app.appName+'_sid' ,
      secret: config.session.secret ,
      store: app.authentication.sessionStore ,
      cookie: {
        path: '/',
        domain: '.'+app.config.host ,
        maxAge: config.session.maxAge || 1000 * 60 * 60 * 24 , // 24 hours
        secure: false
      } ,
      saveUninitialized: false ,
      resave: true
    }));

    return cb();
  };

};
