
// Todo: This should really be passed in as a parameter to a generating function
 // when it is loaded by the parent module
// Todo: Pass in the 

var signedRequest = require('signed-request') ,
    httpSignature = require('http-signature') ,
    md5           = require('md5') ,
    Errors        = require('../errors') ;

function checkMD5Hash(body, expected) {
  var s = (typeof body == 'object') ? JSON.stringify(body) : body;
  var md5body = md5(s);
  console.log('Body MD5: '+md5body);
  return (expected === md5body);
}

module.exports = function checkSignature (req, res, next) {
  // Only check signature if user was authenticated with an API Key
  if (!req.user._authenticatedByAPIKey) return next();

  // If in dev mode, just assume signature is correct. I should think of a better way
    // to do this probably
    
  // if (process.env.NODE_ENV == 'development') {
  //   warp.Logger.verbose('Development mode; ignoring signature check')
  //   return next();
  // }

  var expectedMD5 = req.header('Content-MD5');
  console.log('Expected MD5: '+expectedMD5);
  if (!checkMD5Hash(req.body, expectedMD5)) {
    return next(new Errors.InvalidHashError());
  }

  var pub = req.user.api_secret;

  // Todo: This needs to be a promise so it doesn't block
  try {
    var parsed = httpSignature.parseRequest(req);
    // Todo: Note sure if this is actually comparing to expected; need to confirm
    if (!httpSignature.verifySignature(parsed, pub)) {
      return next(new Errors.InvalidSignatureError());
    }
  } catch (e) {
    return next(e);
  }

  // // console.log('Expected Signature: '+signedRequest.getSignature(req.body, req.user.api_secret))
  // if (!req.header('Hookr-Signature')) {
  //   res.status(400).json({ error: 'No signature provided'})
  // } else if (req.header('Hookr-Signature') != signedRequest.getSignature(req.body, req.user.api_secret) ) {
  //   res.status(400).json({ error: 'Signature does not match'})
  // } else {
  //   next()
  // }

  console.log('leaving checkSign')
  return next();
};
