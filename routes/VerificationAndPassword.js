
var mitch       = require('mitch') ;

var signatureHasher = require('../utils/signature_hash') ;
var Errors = require('../errors');

var debug = require('debug')('mitch:component:authentication:routes:VerificationAndPassword');

module.exports = function (app) {
  
  var config = app.config.authentication;
  var router = mitch.Router();
  
  // Todo: Write a dummy email client which will Log a message instead of sending

  router.get('/verify/email', function (req, res, next) {
    var payload = req.params.payload ;
    var signature = req.params.signature ;

    var expectedSignature = signatureHasher(payload);

    if (signature != expectedSignature) {
      return next(new Errors.InvalidSignatureError());
    } else {
      app.models.User.find({ where: { email: payload.email, verified: false }})
        .then( function (user) {
          user.verified = true;
          user.verified_timestamp = new Date();
          // Todo: Test if I can use promises in the commented way below
          return user.save(['verified', 'verified_timestamp'])
        })
        .then(function() {
          // This should probably actually redirect somewhere
          res.status(200).end();
        }).catch( next );

      /*
      app.models.User.find({ where: { email: payload.email, verified: false }})
        .then( function (user) {
          user.verified = true;
          user.verified_timestamp = new Date();
          return user.save(['verified', 'verified_timestamp'])
        })
        .then(function() {
          // This should probably actually redirect somewhere
          res.status(200).end();
        }).catch( AuthErrors.DatabaseError(res) );
      */
    }

  });

  router.post('/reset/password/start', function (req, res, next) {
    var email = req.body.email;

    app.models.User.find({where: { email: email }})
      .then( function (user) {
        // Send email
        req.status(503).end();
      })
      .catch( next );
  });

  router.post('/reset/password', function (req, res, next) {
    var payload = req.query.payload ;
    var signature = req.query.signature ;

    var expectedSignature = signatureHasher(payload);

    if (signature != expectedSignature) {
      return next(new Errors.InvalidSignatureError());
    } else {

      var password = req.body.password ;

      app.models.User.find({ where: { email: payload.email }})
        .then( function (user) {
          user.password = password;
          return user.save(['password'])
        })
        .then(function() {
          res.status(200).end();
        }).catch( next );
      }
  });

  return router;

};
