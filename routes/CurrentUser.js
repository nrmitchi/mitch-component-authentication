
var mitch       = require('mitch') ;
var signatureHasher = require('../utils/signature_hash') ,
    randomString = require('../utils/random_string') ,
    _ = require('lodash') ;

var debug = require('debug')('mitch:component:authentication:routes:CurrentUser');

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');

module.exports = function (app) {

  var localProtocol = require('../lib/protocols/local')(app);

  var config = app.config.authentication;
  var router = mitch.Router();
  
  // Todo: Default verified = true in development mode
  // Todo: Write endpoint for email verification (stateless)
  
  router.post('/', function (req, res, next) {
    localProtocol.register(req, res, next);
  });

  // Endpoints for current user
  router.get('/', [ app._middleware.requireUser ], function (req, res) {
    res.json({ user: req.user });
  });

  router.get('/:relation', [ app._middleware.requireUser ], function (req, res, next) {
    // Perhaps I should convert to camelCse first, then classify so socialAccount will also work
    var relation = _.classify(req.params.relation);

    if (req.user['get'+relation]) {
      req.user['get'+relation]()
        .then( function (relations) {
          var responseObject = {};
          responseObject[req.params.relation] = relations;
          res.json(responseObject);
        })
        .catch( function () {
          res.status(500).end();
        });
    } else {
      res.status(404).end();
    }
  });

  router.get('/:relation/:id', [ app._middleware.requireUser ], function (req, res, next) {
    // Perhaps I should convert to camelCse first, then classify so socialAccount will also work
    var relation = _.classify(req.params.relation);

    if (req.user['get'+relation]) {
      req.user['get'+relation]({ where: { id : req.params.id} })
        .then( function (relation) {
          res.json(relation);
        })
        .catch( function () {
          res.status(500).end();
        });
    } else {
      res.status(404).end();
    }
  });

  router.put('/', [ app._middleware.requireUser ], function (req, res, next) {
    var attributeWhitelist = ['givenName', 'familyName'];
    req.user.updateAttributes(req.body, attributeWhitelist)
      .then( function (user) {
        res.status(200).json(user);
      })
      .catch( function (err) {
        res.status(500).json(err);
      });
  });

  router.put('/password', [ app._middleware.requireUser ], function (req, res, next) {
    var old_password = req.body.old_password || req.body.password.old ;
    var new_password = req.body.new_password || req.body.password.new ;

    req.user.checkPassword(old_password, function (err, result) {
      if (result) {
        // Old password correct; change password
        req.user.password = new_password;
        req.user.save(['password'])
          .then( function () {
            res.status(200).end();
          })
          .catch( next );
      } else {
        res.status(400).json({ error: 'Incorreect password' });
      }
    });
  });

  router.put('/keys', [ app._middleware.requireUser ], function (req, res, next) {

    // Todo: Switch this to the user.rollKeys method
    req.user.rollKeys();
    req.user.save(['api_key', 'api_secret'])
      .then( function () {
        return res.status(200).end();
      })
      .catch( next );
  });

  router.delete('/', [ app._middleware.requireUser ], function (req, res, next) {
    req.user.destroy()
      .then( function (user) {
        return res.status(204).end();
      })
      .catch( next );
  });

  return router;

};
