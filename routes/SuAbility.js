
var mitch = require('mitch') ;
var _     = require('lodash');
var debug = require('debug')('mitch:component:authentication:routes:SuAbility');

module.exports = function (app) {
  
  var config = app.config;
  var router = mitch.Router();
  
  // Allow a user to pretend to be another user
  // Security: this should be settable by the user
  router.get('/su/:id', [ app._middleware.requireUser ], function (req, res) {
    // ID of the actual current user. If it's not set, the user is on their own account currently
    var superuserPermissionsFunction = _.isFunction(config.userSuAbility) ? config.userSuAbility : function (hostUser, newUser, cb) {
      if (hostUser.superuser) {
        cb(true);
      } else {
        cb(false);
      }
    };

    superuserPermissionsFunction(req.user, null, function (hasPermission) {
      if (hasPermission) {
        req.session.host_user_id = req.session.host_user_id || req.user.id;

        app.models.User.find(req.params.id)
          .then( function (user) {
            if (!user) {
              return res.status(404).end();
            } else {
              req.logIn(user, function(err) {
                if (err) { 
                  debug(err);
                  return res.json(err);
                } 
                // debug(req.user);
                debug('user masquerading as user: '+user.id);
                return res.json(req.user);
              });
            }
          })
          .catch( function (err) {
            return res.status(500).end();
          });
      } else {
        res.status(403).end();
      }
    });
  });
  // Return to the users real account
  // Security: Require req.session.host_user_id to be set
  router.get('/su/exit', [ app._middleware.requireUser ], function (req, res) {
    app.models.User.find(req.session.host_user_id)
      .then( function (user) {
        if (!user) {
          return res.end(404);
        } else {
          req.logIn(user, function(err) {
            if (err) { 
              debug(err);
              return res.json(err);
            } 
            debug('Returning user to own session....');
            return res.json(req.user);
          });
        }
      })
      .catch( function (err) {
        return res.end(500);
      });
  });

  return router;

};
