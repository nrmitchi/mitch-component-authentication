
var LocalStrategy = require('passport-local').Strategy ;
var LocalAPIKeyStrategy = require('passport-localapikey').Strategy ;
var GithubStrategy = require('passport-github').Strategy ;
var BitbucketStrategy = require('passport-bitbucket').Strategy ;

module.exports = {
  eagerLoad: [
    'UserCredential'
  ] ,
  redirectUrl: 'mitch.dev' ,  // The URL which login attempts will be redirected too
  mountPath: '_auth' , // This is the default if it's unincluded
  checkSignatures : true ,
  bcryptPasswordRounds : 10,
  useLegacySessionEndpoints: true ,
  userSuAbility: true , // If true, will use default 'superuser' check. If it is a function, it will use the provided 'function (user1, user1, next) to determine if user1 can take over user2
  loginFailureLimit: 5 ,
  session: {
    host   : process.env.REDIS_SESSION_HOST || '127.0.0.1' ,
    port   : process.env.REDIS_SESSION_PORT || 6379 ,
    auth   : process.env.REDIS_SESSION_AUTH ,
    secret : process.env.SESSION_SECRET ,
    name   : 'mitch_example_sid'
  },
  passport: {
    strategies: {
      local : {
        strategy: LocalStrategy ,
        options: {
          auto_unlocked_account : true ,
          auto_unlocked_account_after : 30000 ,
          module: 'the function thats been loaded' ,
          remember_me : false
        }
      },
      
      localApiKey: {
        strategy: LocalAPIKeyStrategy ,
        nosession: true ,
        options: {
          apiKeyHeader  : 'hookr-api-key' , // Required
          usernameField : 'identifier'      // Required
        }
      },

      // twitter: {
      //   name: 'Twitter',
      //   protocol: 'oauth',
      //   strategy: require('passport-twitter').Strategy,
      //   options: {
      //     consumerKey: 'your-consumer-key',
      //     consumerSecret: 'your-consumer-secret'
      //   }
      // },

      github: {
        name: 'GitHub',
        protocol: 'oauth2',
        strategy: GithubStrategy ,
        options: {
          clientID      : process.env.GITHUB_ACCESS_KEY ,
          clientSecret  : process.env.GITHUB_SECRET_KEY
        },
        scope : 'user:email'
      },

      bitbucket: {
        name: 'Bitbucket',
        protocol: 'oauth',
        strategy: BitbucketStrategy ,
        options: {
          consumerKey     : process.env.BITBUCKET_ACCESS_KEY ,
          consumerSecret  : process.env.BITBUCKET_SECRET_KEY
        }
      }
    }
  }
};
