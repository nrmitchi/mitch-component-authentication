
var userSubscriptions = require('..');
var path = require('path');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var sinon = require('sinon');
require('sinon-as-promised');
var Promise = require('bluebird');
var requireUser = require('../middleware/requireUser');
var checkSignature = require('../middleware/checkSignature');

var count = 0;

describe('authentication middleware', function(){
  var app;

  before(function (done){
    // Set the node directory to the fixture dir so that everything loads properly
    process.chdir(path.join(__dirname, 'fixtures'));

    app = require('./fixtures/app')();

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('requireUser', function() {

    var user;

    before( function (done) {

      count += 1;
      // Create a user
      user = app.models.User.build({
        email: 'test'+count+'@test.com',
        password: 'password',
      })

      user.save().then( function (pet) {
        done();
      }).catch( done );
    });

    it('is successfully mounted', function () {
      should.exist(app._middleware.requireUser)
    });

    it('proceeds when a user is present', function (done) {

      var req = { user: user };
      var res = {};

      requireUser(req, res, function (err) {
        should.not.exist(err);
        done();
      });
    });

    it('proceeds with error when user is missing', function (done) {

      var req = {};
      var res = {};

      requireUser(req, res, function (err) {
        should.exist(err);
        err.name.should.equal('AuthenticationError');
        done();
      });
    });

    it('proceeds with error when user does not have an id', function (done) {

      var req = {
        user: {
          id: undefined
        }
      };
      var res = {};

      requireUser(req, res, function (err) {
        should.exist(err);
        err.name.should.equal('AuthenticationError');
        done();
      });
    });
  });

  describe('checkSignature', function() {

    var user;

    before( function (done) {

      count += 1;
      // Create a user
      user = app.models.User.build({
        email: 'test'+count+'@test.com',
        password: 'password',
      })

      user.save().then( function (pet) {
        done();
      }).catch( done );
    });

    it('is successfully mounted', function () {
      should.exist(app._middleware.checkSignature)
    });

    it('other test');
    it('other other test');
  });

  describe('loadByAccessKey', function() {

    var user;

    before( function (done) {

      count += 1;
      // Create a user
      user = app.models.User.build({
        email: 'test'+count+'@test.com',
        password: 'password',
      })

      user.save().then( function (pet) {
        done();
      }).catch( done );
    });

    it('is successfully mounted', function () {
      should.exist(app._middleware.loadByAccessKey)
    });

    it('other test');
    it('other other test');
  });
});
