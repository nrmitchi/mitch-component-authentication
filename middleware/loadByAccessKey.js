
// Todo: Consider merging the loadByAccessKey and checkSignature functions, and 
  // treat the whole thing as one blackbox function. Then I dont need to add 
  // silly attributes (_authenticatedByAPIKey) or worry about the order
// Todo: This should really be passed in as a parameter to a generating function
 // when it is loaded by the parent module

module.exports = function (app) {
  return function loadByAccessKey (req, res, next) {
    // Only execute if no user already exists
    if (req.user) return next();

    // Todo: I'm not even sure if this is used anymore. Either way, there is already
    //       a header value in the config which can be used here
    if (req.header('Hookr-Access-Key')) {
      app.models.User.find({ where : { api_key : req.header('Hookr-Access-Key') } })
        .then( function (user) {
          req.user = user;
          req.user._authenticatedByAPIKey = true;
          return next();
        })
        .catch( next );
    } else {
      // res.status(400).json({ error: 'No Session or Access Key'});
      return next();
    }
  };
};
