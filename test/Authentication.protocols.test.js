
var userSubscriptions = require('..');
var path = require('path');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var sinon = require('sinon');
require('sinon-as-promised');
var Promise = require('bluebird');
var requireUser = require('../middleware/requireUser');
var checkSignature = require('../middleware/checkSignature');

var count = 0;

describe('authentication protocols', function(){
  var app;

  before(function (done){
    // Set the node directory to the fixture dir so that everything loads properly
    process.chdir(path.join(__dirname, 'fixtures'));

    app = require('./fixtures/app')();

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('basic', function() {
    it('todo: write this test');
  });

  describe('bearer', function() {
    it('todo: write this test');
  });

  describe('cas', function() {
    it('todo: write this test');
  });

  describe('digest', function() {
    it('todo: write this test');
  });

  describe('local', function() {
    describe('register', function () {
      it('todo: write this test');
    });
    
    describe('login', function () {
      it('todo: write this test');
    });
  });

  describe('localApiKey', function() {

    var localApiKey = require('../lib/protocols/localApiKey');

    it('valid key continues with user');
    it('valid key sets _authenticatedByAPIKey');

    it('invalid key provides error in info');
    it('error is passed if user can\'t be queried');
  });

  describe('oauth', function() {
    it('todo: write this test');
  });

  describe('oauth2', function() {
    it('todo: write this test');
  });

  describe('openid', function() {
    it('todo: write this test');
  });

});
