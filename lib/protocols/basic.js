
module.exports = function (app) {
  return function(req, username, password, done) {
    app.models.User.find({
      where: { api_key: username }
    }).then( function (user) {
      if (!user) {
        return done(null, false);
      } else {

        user._authenticatedByAPIKey = true;

        return done(null, user, { _authenticatedByAPIKey : true });
      }
    })
    .catch(done);
  };
};
