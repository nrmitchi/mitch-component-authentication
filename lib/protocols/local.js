var validator = require('validator');
var crypto    = require('crypto');

var Sequelize = require('sequelize');

var debug = require('debug')('mitch:component:authentication:protocols:local');

var passport = require('passport');

/**
 * Local Authentication Protocol
 *
 * The most widely used way for websites to authenticate users is via a username
 * and/or email as well as a password. This module provides functions both for
 * registering entirely new users, assigning passwords to already registered
 * users and validating login requesting.
 *
 * For more information on local authentication in Passport.js, check out:
 * http://passportjs.org/guide/username-password/
 */

module.exports = function (app) {
  var obj = {};
  /**
   * Register a new user
   *
   * This method creates a new user from a specified email, username and password
   * and assign the newly created user a local Passport.
   *
   * @param {Object}   req
   * @param {Object}   res
   * @param {Function} next
   */
  obj.register = function (req, res, next) {
    var email    = req.body.email ,
        username = req.body.username ,
        password = req.body.password ;

    if (!email) {
      req.flash('error', 'Error.Passport.Email.Missing');
      return next(new Error('No email was entered.'));
    }

    if (!password) {
      req.flash('error', 'Error.Passport.Password.Missing');
      return next(new Error('No password was entered.'));
    }

    debug('Creating new user with email: %s', email);
    app.models.User.create({
      first_name : req.body.first_name ,
      last_name  : req.body.last_name ,
      email      : req.body.email ,
      username   : req.body.username ,
      password   : req.body.password
    }).then(function (user) {
      // Todo: Post-create verification emails etc should be added to User model hooks
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }

        return res.json(user);
      });
      return null;
    }).catch(function (err) {
      // Todo: Currently assuming this is a validation error.
      //       Should probably check that
      debug('Error creating User: %s', err);
      if (err instanceof Sequelize.ValidationError) {
        res.status(400).json(err);
      } else if (err instanceof Sequelize.UniqueConstraintError) {
        // Todo: Make this real errors and bubble them up
        res.status(400).json({error: 'UniqueConstraintError: '+err.fields.join(',')});
      } else {
        next(err);
      }
    });
  };

  /**
   * Validate a login request
   *
   * Looks up a user using the supplied identifier (email or username) and then
   * attempts to find a local Passport associated with the user. If a Passport is
   * found, its password is checked against the password supplied in the form.
   *
   * @param {Object}   req
   * @param {string}   identifier
   * @param {string}   password
   * @param {Function} next
   */
  obj.login = function (req, identifier, password, next) {
    var isEmail = validator.isEmail(identifier) ,
        query   = {};

    if (isEmail) {
      query.email = identifier;
    } else {
      query.username = identifier;
    }

    app.models.User.find({
      where: query
    }).then( function (user) {
      if (!user) {
        return next(null, false, { message: 'Incorrect username or password' });
      } else {
        // Check delinquency

        // Check password
        user.checkPassword(password, function (err, result) {
          if (err) {
            return next(null, false, { message: err.message });
          }

          if (result) {
            return next(null, user);
          } else {
            return next(null, false, { message: 'Incorrect username or password' });
          }
        });
      }
    }).catch( next );

  };

  return obj;
  
};
