
var makeError = require('make-error');

function AuthenticationError () {
  AuthenticationError.super.call(this, 'AuthenticationError');
  this.status = 401;
  this.message = 'Unauthorized';
}

function Forbidden () {
  InvalidSignatureError.super.call(this, 'Forbidden');
  this.status = 403;
  this.message = 'Forbidden';
}

function InvalidHashError () {
  InvalidHashError.super.call(this, 'InvalidHashError');
  this.status = 400;
  this.message = 'MD5 Invalid';
}

function InvalidSignatureError () {
  InvalidSignatureError.super.call(this, 'InvalidSignatureError');
  this.status = 400;
  this.message = 'Request Signature Invalid';
}

module.exports = {
  AuthenticationError: makeError(AuthenticationError),
  Forbidden: makeError(Forbidden),
  InvalidHashError: makeError(InvalidHashError),
  InvalidSignatureError: makeError(InvalidSignatureError)
};
