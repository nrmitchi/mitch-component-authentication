/*
 * Bearer Authentication Protocol
 *
 * Bearer Authentication is for authorizing API requests. Once
 * a user is created, a token is also generated for that user
 * in its passport. This token can be used to authenticate
 * API requests.
 *
 */

var passport = require('passport');

module.exports = function (app) {
  return {
    authorize: function(token, done) {
      // Todo: Actually write this. Should use UserCredentials
      app.models.UserCredentials.find({ provider: 'bearer' })
        .then(function (credential) {
          if (!credential) { return done(null, false); }
          return credential.getUser()
            .then(function (user) {
              done(null, user, { scope: 'all' });
            })
        })
        .catch( done );

    }
  };
};
