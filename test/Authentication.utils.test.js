
var userSubscriptions = require('..');
var path = require('path');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var sinon = require('sinon');
require('sinon-as-promised');
var Promise = require('bluebird');
var requireUser = require('../middleware/requireUser');
var checkSignature = require('../middleware/checkSignature');

var count = 0;

describe('authentication utils', function(){
  var app;

  before(function (done){
    // Set the node directory to the fixture dir so that everything loads properly
    process.chdir(path.join(__dirname, 'fixtures'));

    app = require('./fixtures/app')();

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('accountLinker', function() {
    it('todo: write this test');
  });

  describe('randomString', function() {
    it('todo: write this test');
  });

  describe('signatureHash', function() {
    it('todo: write this test');
  });

});
