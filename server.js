
var mitch            = require('mitch') ,
    assert           = require('assert') ,
    async            = require('async') ,
    util             = require('util') ,
    path             = require('path') ,
    
    router           = mitch.Router() ,
    flash            = require('connect-flash') ,
    expressSession   = require('express-session') ,
    passport         = require('passport') ,
    
    loadMiddleware   = require('./lib/loadMiddleware') ,
    loadPassport     = require('./lib/loadPassportModules') ,
    loadModels       = require('./lib/loadModels') ,
    loadSessionStore = require('./lib/loadSessionStore') ;
    
module.exports = function (app) {
  
  assert(app.config.authentication, 'Authentication configuration must exist and be loaded');

  // Create somewhere on app for the module to save stuff (that typically only applies to it)
  app.authentication = app.authentication || {};

  loadSessionStore(app, app.config.authentication)();
  loadMiddleware(app)();
  loadModels(app, app.config.authentication)();
  loadPassport(app, app.config.authentication)();

  // Note: This also loads middleware/serialization/etc into the main app
  //       These two sections (middleware vs creating router) 
  //       should probably be split

  var sessionRouter                 = require('./routes/Session')(app);
  var currentUserRouter             = require('./routes/CurrentUser')(app);
  var verificationAndPasswordRouter = require('./routes/VerificationAndPassword')(app);
  var suAbilityRouter               = require('./routes/SuAbility')(app);
  var passportRouter                = require('./routes/Passport')(app);

  router.use('/user',     currentUserRouter);
  router.use('/session',  sessionRouter);
  router.use('/',         verificationAndPasswordRouter);
  if (app.config.authentication.userSuAbility) { 
    router.use('/user',   suAbilityRouter);
  }
  // Note this one has to be last, or it will highjack /_auth/* due to whitelist
  router.use('/',         passportRouter);

  return router;
};
