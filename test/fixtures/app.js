
var _ = require('lodash');
var mitch = require('mitch');

module.exports = function createApp () {

  var app = mitch();

  app.config = {};
  _.extend(app.config, {
    sequelize: {
      host: 'localhost',
      username: 'nrmitchi',
      password: 'asdF456^',
      database: 'mitch-test',
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
      logging: false
    },
    globals: {}
  });

  // Todo: This needs to be the full authentication config
  _.extend(app.config, {
    authentication: {
      eagerLoad: [
        'UserCredential'
      ] ,
      redirectUrl: 'mitch.dev' ,  // The URL which login attempts will be redirected too
      mountPath: '_auth' , // This is the default if it's unincluded
      checkSignatures : true ,
      bcryptPasswordRounds : 10,
      useLegacySessionEndpoints: true ,
      userSuAbility: true , // If true, will use default 'superuser' check. If it is a function, it will use the provided 'function (user1, user1, next) to determine if user1 can take over user2
      loginFailureLimit: 5 ,
      session: {
        host   : process.env.REDIS_SESSION_HOST || '127.0.0.1' ,
        port   : process.env.REDIS_SESSION_PORT || 6379 ,
        auth   : process.env.REDIS_SESSION_AUTH ,
        secret : process.env.SESSION_SECRET ,
        name   : 'mitch_example_sid'
      },
      passport: {
        strategies: {
          // local : {
          //   strategy: LocalStrategy ,
          //   options: {
          //     auto_unlocked_account : true ,
          //     auto_unlocked_account_after : 30000 ,
          //     module: 'the function thats been loaded' ,
          //     remember_me : false
          //   }
          // },

          // localApiKey: {
          //   strategy: LocalAPIKeyStrategy ,
          //   nosession: true ,
          //   options: {
          //     apiKeyHeader  : 'hookr-api-key' , // Required
          //     usernameField : 'identifier'      // Required
          //   }
          // },

          // github: {
          //   name: 'GitHub',
          //   protocol: 'oauth2',
          //   strategy: GithubStrategy ,
          //   options: {
          //     clientID      : process.env.GITHUB_ACCESS_KEY ,
          //     clientSecret  : process.env.GITHUB_SECRET_KEY
          //   },
          //   scope : 'user:email'
          // },

          // bitbucket: {
          //   name: 'Bitbucket',
          //   protocol: 'oauth',
          //   strategy: BitbucketStrategy ,
          //   options: {
          //     consumerKey     : process.env.BITBUCKET_ACCESS_KEY ,
          //     consumerSecret  : process.env.BITBUCKET_SECRET_KEY
          //   }
          // }
        }
      }
    }
  });

  app.loadMiddleware();
  app._middleware = app._middleware || {};

  app.loadUserModules();

  app.loadModels();

  // This is only used for properly checking errors in error cases.
  // In a real app this would be handled at a higher level
  app.use(function (err, req, res, next) {
    res.status(500).json({
      type: err.name,
      message: err.message,
      stack: err.stack
    });
  });

  return app;

};
