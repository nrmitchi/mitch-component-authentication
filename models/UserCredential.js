
var underscore = require('lodash') ;

var debug = require('debug')('mitch:component:authentication:model:UserCredential');

module.exports = function (app) {
  return function(sequelize, DataTypes) {
  
    var modelAttributes = {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      // user_id: {
      //   type: DataTypes.INTEGER
      // },
      provider: {
        type: DataTypes.STRING
      },
      provider_id: {
        type: DataTypes.STRING
      },
      protocol: {
        type: DataTypes.STRING
      },
      credentials: {
        type: DataTypes.JSON
      },
      // access_token: {
      //   type: DataTypes.STRING
      // },
      // refresh_token: {
      //   type: DataTypes.STRING
      // }
      /*
      // Todo: Would this be a good idea?
      profile: {
        type: DataTypes.JSON // (in fact, PGJson)
      }
      */
    };

    // The attributes in this model should be build from the config
    var UserCredential = sequelize.define('UserCredential', modelAttributes, {
      paranoid: true ,
      tableName: 'user_credentials' ,
      comment: 'UserCredential database' ,
      associate: function(models) {
        UserCredential.belongsTo(models.User, { foreignKey: 'user_id' });
      },
      classMethods: {
        // attributeWhitelist: function(){ return [] }
      },
      instanceMethods: {
        // Note: cb will receive (err, Boolean)
        // getFullname: function() {
        //   return [this.first_name, this.last_name].join(' ');
        // }
      }
    });

    UserCredential.beforeValidate( function(credential, options, fn) {
      debug('beforeValidate - UserCredential');
      return fn(null, credential);
    });
    UserCredential.afterValidate( function(credential, options, fn) {
      debug('afterValidate - UserCredential');
      return fn(null, credential);
    });
    UserCredential.beforeCreate( function(credential, options, fn) {
      debug('beforeCreate - UserCredential');
      return fn();
    });
    UserCredential.afterCreate( function(credential, options, fn) {
      debug('afterCreate - UserCredential');
      return fn();
    });
    UserCredential.beforeUpdate( function(credential, options, fn) {
      debug('beforeUpdate - UserCredential');
      // If password has changed, rehash it
      return fn();
    });
    UserCredential.afterUpdate( function(credential, options, fn) {
      debug('afterUpdate - UserCredential');
      return fn();
    });
    UserCredential.beforeDestroy( function(credential, options, fn) {
      debug('beforeDestroy - UserCredential');
      return fn();
    });
    UserCredential.afterDestroy( function(credential, options, fn) {
      debug('afterDestroy - UserCredential');
      return fn();
    });

    return UserCredential;

  };
};