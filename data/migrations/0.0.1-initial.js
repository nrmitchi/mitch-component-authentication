
var async = require('async') ,
    _     = require('lodash') ;

function wrapAsync (t) {
  return function (callback) {
    t.then( function () {
      callback();
    }, function (err) {
      callback(err);
    });
  };
}

module.exports = {
  up: function(migration, DataTypes, next) {
    // add altering commands here, calling 'done' when finished
    var tasks = [
      migration.createTable(
        'logins',
        {
          id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          
          provider: {
            type: DataTypes.STRING,
            allowNull: false
          },
          ip: {
            type: DataTypes.STRING
          },
          user_agent: {
            type: DataTypes.STRING
          },
          
          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          },
          deletedAt: {
            type: DataTypes.DATE
          }
        },
        {}
      ),
      migration.createTable(
      	'remember_me_tokens',
      	{
      		id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          
          token: {
            type: DataTypes.STRING,
            allowNull: false
          },

          user_agent: {
            type: DataTypes.STRING
          },
          
          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          },
          deletedAt: {
            type: DataTypes.DATE
          }
      	},
      	{}
      ),
      migration.createTable(
      	'sessions',
      	{
      		id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          
          ip: {
            type: DataTypes.STRING,
            allowNull: false
          },
          user_agent: {
            type: DataTypes.STRING
          },
          
          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          },
          deletedAt: {
            type: DataTypes.DATE
          }
      	},
      	{}
      ),
      migration.createTable(
      	'user_credentials',
      	{
      		id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          
          provider: {
            type: DataTypes.STRING,
            allowNull: false
          },
          provider_id: {
            type: DataTypes.STRING,
            allowNull: false
          },
          access_token: {
            type: DataTypes.STRING,
            allowNull: false
          },
          refresh_token: {
            type: DataTypes.STRING,
            allowNull: false
          },
          
          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          },
          deletedAt: {
            type: DataTypes.DATE
          }
      	},
      	{}
      ),
      migration.createTable(
      	'users',
      	{
      		id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          
          givenName: {
            type: DataTypes.STRING
          },
          familyName: {
            type: DataTypes.STRING
          },
          username: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
          },
          email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
          },
          password: {
            type: DataTypes.STRING,
            allowNull: false
          },

          api_key: {
            type: DataTypes.STRING,
            allowNull: false
          },
          api_secret: {
            type: DataTypes.STRING,
            allowNull: false
          },
          
          admin: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
          },
          superuser: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
          },
          active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
          },
          verified: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
          },
          verified_timestamp: {
            type: DataTypes.DATE
          },

          last_login: {
            type: DataTypes.DATE
          },
          login_count: {
            type: DataTypes.INTEGER,
            defaultValue: 0
          },

          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          },
          deletedAt: {
            type: DataTypes.DATE
          }
      	},
      	{}
      )
    ];


    async.series(_.map(tasks, wrapAsync), function (err) {
      if (err) console.log(err);
      next(err);
    });
  },
  down: function(migration, DataTypes, next) {
    // add reverting commands here, calling 'done' when finished
    var tasks = [
      // migration.dropTable('logins'),
      // migration.dropTable('remember_me_tokens'),
      // migration.dropTable('sessions'),
      migration.dropTable('user_credentials'),
      migration.dropTable('users'),
    ];

    async.series(_.map(tasks, wrapAsync), function (err) {
      if (err) console.log(err);
      next(err);
    });

  }
};
