
var Errors = require('../errors');

module.exports = function requireUser (req, res, next) {
	if (req.user && req.user.id) {
		return next();
	} else {
    return next(new Errors.AuthenticationError());
	}
};
