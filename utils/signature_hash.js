
var crypto = require('crypto') ;

module.exports = function (payload) {
  // Todo: Config this
  var secret = 'hashsecretthatprobablyshouldbereplaced';

  if (typeof payload == 'object') payload = JSON.stringify(payload);
  var shasum = crypto.createHash('sha1');
  shasum.update(secret+payload);
  return shasum.digest('hex');
};
